package Comparable_Comparator;

import java.util.Comparator;

public class ProfitComparator implements Comparator<Company>   {

	@Override
	public int compare(Company o1, Company o2) {
		// TODO Auto-generated method stub
		double b1 = o1.getProfit();
		double b2 = o2.getProfit();
		if(b1 < b2) return -1;
		if(b1 > b2) return 1;
		return 0;
	}
	

}
