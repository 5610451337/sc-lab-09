package Comparable_Comparator;

public interface Taxable {
	double getTax();
	String getName();
	
}
