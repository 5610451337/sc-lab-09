package Comparable_Comparator;

public class Product implements Comparable<Product>,Taxable {
	
	private String name;
	private double price;
	private double tax;


	public Product(String aname,double aPrice){
		name=aname;
		price=aPrice;
	}
	public String getName(){
		return name;
	}
	public double getPrice(){
		return price;
	}
	@Override
	public int compareTo(Product other) {
		if (this.price < other.price ) { return -1; }
		if (this.price > other.price ) { return 1;  }
		return 0;
	}
	@Override
	public double getTax() {
		tax=price*0.07;
		return tax;
	}
	

}
