package Comparable_Comparator;


public class Person implements Comparable<Person>,Taxable   {
	private String name;
	private double salary;
	private double tax;


	public Person(String aname,double aSalary){
		name=aname;
		salary=aSalary;
	}
	public String getName(){
		return name;
	}
	public double getSalary(){
		return salary;
	}
	@Override
	public int compareTo(Person other) {
		if (this.salary < other.salary ) { return -1; }
		if (this.salary > other.salary ) { return 1;  }

		return 0;
	}
	@Override
	public double getTax() {
		if(salary<300000){
			tax=salary*0.05;
		}
		else{
			tax=((salary-300000)*0.1)+15000;
		}
		return tax;
	}

	

}

