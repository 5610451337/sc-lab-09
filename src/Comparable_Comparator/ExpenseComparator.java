package Comparable_Comparator;

import java.util.Comparator;

public class ExpenseComparator implements Comparator<Company>  {

	@Override
	public int compare(Company o1, Company o2) {
		double b1 = o1.getExpense();
		double b2 = o2.getExpense();
		if (b1 < b2) return -1;
		if (b1 > b2) return 1;

		return 0;
	}

}
