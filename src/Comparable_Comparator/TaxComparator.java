package Comparable_Comparator;

import java.util.Comparator;

public class TaxComparator implements Comparator<Taxable> {

	@Override
	public int compare(Taxable o1, Taxable o2) {
		double b1 = o1.getTax();
		double b2 = o2.getTax();
		if (b1 < b2) return -1;
		if (b1 > b2) return 1;

		return 0;
	}

}
