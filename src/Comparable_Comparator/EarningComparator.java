package Comparable_Comparator;

import java.util.Comparator;

public class EarningComparator implements Comparator<Company> {

	@Override
	public int compare(Company o1, Company o2) {
		double b1 = o1.getEarning();
		double b2 = o1.getEarning();
		if (b1 < b2) return -1;
		if (b1 > b2) return 1;

		return 0;
	}
	

}
