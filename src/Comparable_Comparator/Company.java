package Comparable_Comparator;

public class Company implements Taxable   {
	private String name;
	private double earning;
	private double expense;
	private double profit;
	private double tax;
	
	public Company(String aname,double aearning,double aexpense){
		name =aname;
		earning = aearning;
		expense = aexpense;
		profit = aearning-aexpense;
		
	}
	public String getName(){
		return name;
	}
	public double getEarning(){
		return earning;
	}
	public double getExpense(){
		return expense;
	}
	public double getProfit(){
		return profit;
	}
	
	public String toString() {
		return "Company[name="+this.name+", Earning="+this.earning+", Expense="+this.expense+", Profit="+this.profit+"]";
	}
	@Override
	public double getTax() {
		if(earning>expense){
			tax=(earning-expense)*0.3;
		}
		else{
			tax=0;
		}
		return tax;// TODO Auto-generated method stub

	}
	

}
