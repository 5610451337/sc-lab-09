package Comparable_Comparator;

import java.util.ArrayList;
import java.util.Collections;

public class Test_Case {
	public static void main(String[] args){
		// ------------------- Person ---------------------
		ArrayList<Person> persons  = new ArrayList<Person>();
		persons.add(new Person("Wut",1340000));
		persons.add(new Person("Black",3000));
		persons.add(new Person("Mark",2000000));
		System.out.println("---before sort Saraly Person");
		for(Person p : persons)
			System.out.println(p.getName()+"  "+p.getSalary());
		Collections.sort(persons);
		System.out.println("\n---after sort Saraly Person");
		for(Person p : persons)
			System.out.println(p.getName()+"  "+p.getSalary());
		
		// ------------------- Product ---------------------
		
		ArrayList<Product> products = new ArrayList<Product>();
		products.add(new Product("Pen",12));
		products.add(new Product("Pencil",15));
		products.add(new Product("Eraser",10));
		System.out.println("---before sort Price Products");
		for(Product p : products)
			System.out.println(p.getName()+"  "+p.getPrice());
		Collections.sort(products);
		System.out.println("\n---after sort Price Products");
		for(Product p : products)
			System.out.println(p.getName()+"  "+p.getPrice());
		
		
		// ------------------- Company ---------------------
		
		ArrayList<Company> companies = new ArrayList<Company>();
		companies.add(new Company("Black and Boss co,Ltd",100000,1000));
		companies.add(new Company("Mark and Benz co,Ltd",500000,5000));
		companies.add(new Company("Wut and Aom co,Ltd",2000000,3000));
		
		System.out.println("---- Before sort");
		
		for(Company c :companies){
			System.out.println(c.toString());
		}
		Collections.sort(companies, new EarningComparator());
		System.out.println("\n---- After sort with Earing");
		for(Company c :companies){
			System.out.println(c.toString());
		}
		Collections.sort(companies, new ExpenseComparator());
		System.out.println("\n---- After sort with Expense");
		for(Company c :companies){
			System.out.println(c.toString());
		}
		Collections.sort(companies, new ProfitComparator());
		System.out.println("\n---- After sort with Profit");
		for(Company c :companies){
			System.out.println(c.toString());
		}
		
		
		// --------------- Person, Product, Company -----------------
		ArrayList<Taxable> tax = new ArrayList<Taxable>();
		tax.add(new Person("Boss",10000000));
		tax.add(new Product("Laptop",50000));
		tax.add(new Company("Wut and Aom co,Ltd",2000000,3000));
		
		System.out.println("---- Before sort");
		
		for(Taxable t :tax){
			System.out.println(t.getName()+" "+t.getTax());
		}
		
		Collections.sort(tax, new TaxComparator());
		
		System.out.println("\n---- After sort with Tax");
		for(Taxable t :tax){
			System.out.println(t.getName()+" "+t.getTax());
		}
		
		
		
	}

}
