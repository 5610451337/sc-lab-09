package Tree_Traversal;

import java.util.ArrayList;
import java.util.List;

public class PostOrderTraversal implements Traversal {

	@Override
	public List<Node> traverse(Node node) {
		// TODO Auto-generated method stub
		List<Node> tree;
		if(node==null){
			return new ArrayList<Node>();
		}
		else{
			tree= traverse(node.getLeft());
			tree.addAll(traverse(node.getRight()));
			tree.add(node);
			return tree;
		}
	}

}
