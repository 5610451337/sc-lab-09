package Tree_Traversal;

public class Node {
	private String name;
	private Node left;
	private Node right;
	
	public Node(String aname,Node aleft,Node aright ){
		name=aname;
		left=aleft;
		right=aright;
			
	}
	public String getName(){
		return name;
	}
	public Node getLeft(){
		return left;
		
	}
	public Node getRight(){
		return right;
	}

}
